package publications;

import formatters.BookFormatter;
import interfaces.ICitable;
import interfaces.IBookFormatter;

public class Book extends Publication {
    private String isbn;
    private String publisher;
    private Integer volume;
    private String url;
    private final IBookFormatter formatter;

    public Book(String id, String author, String doi, String title, String url, String isbn, String publisher, Integer volume) {
        super(id, author, doi, title, url);
        this.isbn = isbn;
        this.publisher = publisher;
        this.volume = volume;
        this.url = url;
        this.formatter = new BookFormatter();
    }

    public Book(String id, String author, String doi, String title, String url, String isbn, String publisher, Integer volume, IBookFormatter formatter) {
        super(id, author, doi, title, url);
        this.isbn = isbn;
        this.publisher = publisher;
        this.volume = volume;
        this.url = url;
        this.formatter = formatter;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getPublisher() {
        return publisher;
    }

    public Integer getVolume() {
        return volume;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String getCitation() {
        return formatter.format(this);
    }
}
