package publications;

import formatters.ArticleFormatter;
import interfaces.IArticleFormatter;

public class Article extends Publication {
    private Integer month;
    private String pages;
    private Integer year;
    private String journal;
    private String issn;
    private Integer issue;
    private final IArticleFormatter formatter;

    public Article(String id, String author, String doi, String title, String url, Integer month, String pages, Integer year, String journal, String issn, Integer issue) {
        super(id, author, doi, title, url);
        this.month = month;
        this.pages = pages;
        this.year = year;
        this.journal = journal;
        this.issn = issn;
        this.issue = issue;
        this.formatter = new ArticleFormatter();
    }

    public Article(String id, String author, String doi, String title, String url, Integer month, String pages, Integer year, String journal, String issn, Integer issue, IArticleFormatter formatter) {
        super(id, author, doi, title, url);
        this.month = month;
        this.pages = pages;
        this.year = year;
        this.journal = journal;
        this.issn = issn;
        this.issue = issue;
        this.formatter = formatter;
    }

    public Integer getMonth() {
        return month;
    }


    public String getPages() {
        return pages;
    }

    public Integer getYear() {
        return year;
    }

    public String getJournal() {
        return journal;
    }

    public String getIssn() {
        return issn;
    }

    public Integer getIssue() {
        return issue;
    }


    @Override
    public String getCitation() {
        return formatter.format(this);
    }
}
