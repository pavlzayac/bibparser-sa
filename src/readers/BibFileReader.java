package readers;

import interfaces.IReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Scanner;

public class BibFileReader implements IReader {
    private String path;

    public BibFileReader(String path) {
        this.path = path;
    }

    @Override
    public List<String> getLines() {
        try {
            Path filePath = Path.of(path);
            List<String> lines = Files.readAllLines(filePath);
            return lines;
        } catch (IOException e) {
            System.out.println("An exception occured when trying to read the file: check the file path.");
        }

        return List.of();
    }
}
