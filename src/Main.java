import parsers.BibParser;
import publications.Publication;
import readers.BibFileReader;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        BibFileReader fr = new BibFileReader("test.bib");
        BibParser parser = new BibParser();

        List<Publication> pubs = parser.parse(fr);

        for (Publication p : pubs) {
            System.out.println(p.getCitation());
        }
    }
}