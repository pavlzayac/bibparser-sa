package parsers;

import interfaces.IParser;
import interfaces.IReader;
import publications.Article;
import publications.Book;
import publications.Publication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BibParser implements IParser {
    @Override
    public List<Publication> parse(IReader reader) {
        List<Publication> publications = new ArrayList<>();

//        Publication currentPublication = null;
        String publicationType = "";
        Map<String, String> attributes = new HashMap<>();

        for (String line : reader.getLines()) {
            if (line.startsWith("@")) {
                attributes.clear();
                String[] typeAndId = line.strip().split("[@,{]");
                publicationType = typeAndId[1];
                attributes.put("id", typeAndId[2]);
            } else if (line.startsWith("}")) {
                switch (publicationType) {
                    case "article":
                        publications.add(parseArticle(attributes));
                        break;
                    case "book":
                        publications.add(parseBook(attributes));
                        break;
                    default:
                        System.out.println("Invalid item: the item type is not a Book or Article.");
                }
            } else {
                String[] fieldNameAndContent = line.split("=");
                String fieldName = fieldNameAndContent[0].strip();
                String content = fieldNameAndContent[1].strip().substring(1, fieldNameAndContent[1].length() - 3);

                attributes.put(fieldName, content);
            }

        }

        return publications;
    }

    private Article parseArticle(Map<String, String> fields) {
        return new Article(
                fields.get("id"),
                fields.get("author"),
                fields.get("doi"),
                fields.get("title"),
                fields.get("url"),
                fields.get("month") == null ? null : Integer.parseInt(fields.get("month")),
                fields.get("pages"),
                fields.get("year") == null ? null : Integer.parseInt(fields.get("year")),
                fields.get("journal"),
                fields.get("issn"),
                fields.get("issue") == null ? null : Integer.parseInt(fields.get("issue"))
        );

    }

    private Book parseBook(Map<String, String> fields) {
        return new Book(
                fields.get("id"),
                fields.get("author"),
                fields.get("doi"),
                fields.get("title"),
                fields.get("url"),
                fields.get("isbn"),
                fields.get("publisher"),
                fields.get("volume") == null ? null : Integer.parseInt(fields.get("volume"))
        );
    }
}
