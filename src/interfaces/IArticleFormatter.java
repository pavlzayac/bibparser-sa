package interfaces;

import publications.Article;

public interface IArticleFormatter {
    String format(Article article);
}
