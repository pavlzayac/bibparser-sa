package interfaces;

public interface ICitable {
    String getCitation();
}
